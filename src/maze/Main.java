package maze;

import java.util.*;

public class Main {

    private static final String DEFAULT_MAZE = "011001100011111\n100111011100111\n011000101110011\n110111101101100\n" +
            "110100101101111\n101101101100101\n011110011111011\n101101101111110\n" +
            "110011101100001\n001111010011110\n010011011111110\n";
    private static final int MAZE_WIDTH = 15;
    private static final int MAZE_HEIGHT = 11;

    public static void main(String[] args) {
        //String mazeStr = generateRandomMatrix(MAZE_WIDTH, MAZE_HEIGHT);
        Maze maze = new Maze(DEFAULT_MAZE, MAZE_WIDTH, MAZE_HEIGHT);
        System.out.printf("=====\n%s=====\n", maze.getMazeStr());
        if (!maze.solve()) {
            System.err.println("WARN: There are no solutions for this maze.");
        } else {
            System.out.printf("INFO: Hey! I solved the maze for you. Here: %s\n", maze.getSolutions());
        }
    }

    @SuppressWarnings("unused")
    private static String generateRandomMatrix(int width, int height) {
        StringBuilder maze = new StringBuilder();
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if ((i == 0 && j == 0) || (i == height - 1 && j == width - 1)) {
                    maze.append("0");
                } else {
                    maze.append(new Random().nextBoolean() ? "0" : "1");
                }
            }
            maze.append("\n");
        }
        return maze.toString();
    }
}
