package maze;

class XNode<T> {
    private final T data;
    private XNode<T> next;

    XNode(T data) {
        this.data = data;
        this.next = null;
    }

    T getData() {
        return data;
    }

    XNode<T> getNext() {
        return next;
    }

    void setNext(XNode<T> next) {
        this.next = next;
    }
}
