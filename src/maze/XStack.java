package maze;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class XStack<T> {
    private XNode<T> top = null;

    public void push(T data) {
        XNode<T> node = new XNode<T>(data);
        node.setNext(this.top);
        this.top = node;
    }

    public T pop() {
        if(this.isEmpty()) {
            return null;
        }
        T data = this.top.getData();
        top = top.getNext();
        return data;
    }

    public boolean isEmpty() {
        return this.top == null;
    }

    public List<T> asList() {
        List<T> list = new ArrayList<>();
        while (!this.isEmpty()) {
            list.add(this.pop());
        }
        Collections.reverse(list);
        return list;
    }
}
