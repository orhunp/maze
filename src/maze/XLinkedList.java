package maze;

public class XLinkedList<T> {

    private XNode<T> head;

    XLinkedList() {
        this.head = null;
    }

    public int size() {
        int size = 0;
        XNode<T> current = head;
        while (current != null) {
            size++;
            current = current.getNext();
        }
        return size;
    }

    public boolean isEmpty() {
        return this.size() == 0;
    }

    public void push(T newEntry) {
        XNode<T> node = new XNode<>(newEntry);
        if (head == null) {
            head = node;
        } else {
            XNode<T> last = head;
            while (last.getNext() != null) {
                last = last.getNext();
            }
            last.setNext(node);
        }
    }

    public T remove() {
        if (this.head == null)
            return null;
        if (this.head.getNext() == null) {
            T last = this.head.getData();
            this.clear();
            return last;
        }
        XNode<T> sec_last = this.head;
        while (sec_last.getNext().getNext() != null)
            sec_last = sec_last.getNext();
        T last = sec_last.getNext().getData();
        sec_last.setNext(null);
        return last;
    }

    public void clear() {
        this.head = null;
    }
}
