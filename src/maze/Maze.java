package maze;

import java.util.ArrayList;
import java.util.List;

public class Maze {

    private static final Point FIRST_POINT = new Point(0, 0);
    private static final int[][] DIRECTIONS = {{-1, -1}, {0, -1}, {1, -1}, {1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0}};
    private final int width, height;
    private final String mazeStr;
    private final Point[][] maze;
    private XStack<List<Point>> solutions;

    public Maze(String mazeStr, int width, int height) {
        this.mazeStr = mazeStr;
        this.width = width;
        this.height = height;
        this.maze = this.process();
    }

    public String getMazeStr() {
        return mazeStr;
    }

    public boolean solve() {
        XStack<List<Point>> solutions = new XStack<>();
        List<Point> firstSolution = solveIt(FIRST_POINT, null);
        if (firstSolution != null) {
            solutions.push(firstSolution);
            for (int i = 1; i < firstSolution.size(); i++) {
                try {
                    List<Point> solution, extendedHistory = new ArrayList<>();
                    extendedHistory.add(firstSolution.get(i));
                    solution = solveIt(firstSolution.get(i + 1), extendedHistory);
                    if (solution != null) {
                        solutions.push(solution);
                    }
                } catch (IndexOutOfBoundsException e) {
                    // suppress
                }
            }
        }
        this.solutions = solutions;
        return !this.solutions.isEmpty();
    }

    public String getSolutions() {
        StringBuilder solutionsStr = new StringBuilder();
        for (List<Point> solution : this.solutions.asList()) {
            solutionsStr.append("\n=====\n");
            solutionsStr.append(this.getSolution(solution));
            solutionsStr.append("=====");
        }
        return solutionsStr.toString();
    }

    private List<Point> solveIt(Point entryPoint, List<Point> history) {
        XLinkedList<Point> routes = new XLinkedList<>();
        routes.push(entryPoint);
        if (history == null) {
            history = new ArrayList<>();
        }
        while (!routes.isEmpty()) {
            Point current = routes.remove();
            boolean discovered = false;
            for (Point point: history) {
                if (point.getX() == current.getX() && point.getY() == current.getY()) {
                    discovered = true;
                    break;
                }
            }
            if (discovered) {
                continue;
            }
            try {
                if (!this.maze[current.getX()][current.getY()].getFlag()) {
                    history.add(current);
                    continue;
                }
            } catch (IndexOutOfBoundsException e) {
                continue;
            }
            if (current.getX() == width - 1 && current.getY() == height - 1) {
                List<Point> solution = new ArrayList<>(), uniqueValues = new ArrayList<>();
                Point iterator = current;
                while (iterator != null) {
                    for (Point point: uniqueValues) {
                        if (point.getX() == iterator.getX() && point.getY() == iterator.getY()) {
                            return null;
                        }
                    }
                    solution.add(iterator);
                    uniqueValues.add(iterator);
                    iterator = iterator.getPrev();
                }
                return solution;
            }
            for (int[] direction : DIRECTIONS) {
                Point point = new Point(current.getX() + direction[0], current.getY() + direction[1], current);
                history.add(current);
                routes.push(point);
            }
        }
        return null;
    }

    private String getSolution(List<Point> routes) {
        StringBuilder maze = new StringBuilder();
        for (int i = 0; i < this.mazeStr.split("\n").length; i++) {
            for (int j = 0; j < this.mazeStr.split("\n")[i].length(); j++) {
                boolean routeFound = false;
                for (Point point : routes) {
                    if (point.getX() == j && point.getY() == i) {
                        maze.append("#");
                        routeFound = true;
                        break;
                    }
                }
                if (!routeFound) {
                    maze.append(".");
                }
            }
            maze.append("\n");
        }
        return maze.toString();
    }

    private Point[][] process() {
        Point[][] maze = new Point[this.width][this.height];
        for (int i = 0; i < this.width; i++) {
            for (int j = 0; j < this.height; j++) {
                boolean x = String.valueOf(this.mazeStr.split("\n")[j].charAt(i)).equals("0");
                maze[i][j] = new Point(i, j, x);
            }
        }
        return maze;
    }
}
