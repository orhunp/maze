package maze;

class Point {
    private final int x, y;
    private boolean flag;
    private Point prev;

    public Point(int x, int y, boolean flag) {
        this.x = x;
        this.y = y;
        this.flag = flag;
    }

    public Point(int x, int y, Point prev) {
        this.x = x;
        this.y = y;
        this.prev = prev;
    }

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean getFlag() {
        return flag;
    }

    public Point getPrev() {
        return prev;
    }
}
